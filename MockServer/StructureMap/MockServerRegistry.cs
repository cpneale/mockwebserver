﻿using log4net;
using MockServer.Configuration;
using MockServer.Loggers;
using MockServer.Providers.WireMockProxy;
using MockServer.Services;

using StructureMap;
using WireMock.Logging;
using WireMock.Settings;
using FluentMockServerSettings = MockServer.Configuration.FluentMockServerSettings;

namespace MockServer.StructureMap
{
    public class MockServerRegistry : Registry
    {
        public MockServerRegistry()
        {
            For<IHttpResponseService>()
                .Use<HttpResponseService>();

            For<IWireMockClient>()
                .Use<WireMockClient>()
                .Singleton();

            For<IFluentMockServerSettings>()
                .Use<FluentMockServerSettings>()
                .Singleton();

            For<IConfigurationReader>()
                .Use<ConfigurationReader>()
                .Singleton();

            For<ILog>()
                .Use(c => LogManager.GetLogger(c.ParentType ?? typeof(ILog)))
                .AlwaysUnique();

            For<IWireMockLogger>()
                .Use<Log4NetLogger>();
        }
    }

}