﻿using System;
using System.IO;
using System.Net.Http;
using MockServer.Loggers;
using MockServer.Providers.Exceptions;
using MockServer.Providers.Helpers;
using WireMock.Logging;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;


namespace MockServer.Providers.WireMockProxy
{
    public class WireMockClient : IDisposable, IWireMockClient
    {
        private static FluentMockServer _server;
        private IFluentMockServerSettings _settings;
        private IWireMockLogger _logger;
        private static string LocalHostAndPort { get; set; }

        public WireMockClient(IFluentMockServerSettings settings, IWireMockLogger logger)
        {
            _settings = settings;
            _logger = logger;
            Initialize();
        }

        private void Initialize()
        {
            //going to start as a singleton but just in case someone decides to instantiate a lot of these
            if (_server != null)
                return;

            StartWireMock();

            if (_server != null && _server.IsStarted)
            {
                LocalHostAndPort = $"http://localhost:{_server?.Ports[0]}";
                ConfigureWireMock();
                TestWireMock();
            }
        }

        public HttpResponseMessage GetResponse(HttpRequestMessage requestMessage)
        {
            try
            {
                var client = new HttpClient();

                string absolutePath = requestMessage.RequestUri.AbsolutePath.ToLower();
                string virtualPathRoot = requestMessage.GetRequestContext().VirtualPathRoot.ToLower();

                string wireMockPath =
                    virtualPathRoot != null ?
                    absolutePath.Replace(virtualPathRoot, string.Empty) :
                    absolutePath;

                Uri wireMockUri = BuildUri(wireMockPath);
                HttpRequestMessage clonedRequest = HttpRequestHelper.CloneHttpRequestMessage(requestMessage, wireMockUri);

                return client.SendAsync(clonedRequest).Result;
            }
            catch (Exception ex)
            {
                throw new WireMockMappingException(ex.Message, ex);
            }
        }


        private void StartWireMock()
        {
            _settings.Logger = _logger;
            _server = FluentMockServer.Start(_settings);
        }

        public void ConfigureWireMock()
        {
            // Health check
            _server.Given(Request.Create()
                                .UsingGet()
                                .WithPath("/healthcheck")
                  )
                  .WithTitle("healthcheck")
                  .RespondWith(Response.Create()
                                       .WithSuccess()
                                       .WithBodyAsJson("{'Result':'success'}")
                                       .WithHeader("Access-Control-Allow-Origin", "*")
                  );
        }

        private void TestWireMock()
        {
            var client = new HttpClient();
            Uri uri = BuildUri("/healthcheck");

            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            HttpResponseMessage testResponse = client.SendAsync(request).Result;

            if (!_server.IsStarted || testResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApplicationException("Error starting Mock Server");
            }
        }

        private Uri BuildUri(string path)
        {
            return new Uri($"{LocalHostAndPort}{path}");
        }

        public void Dispose()
        {
            if (_server != null && _server.IsStarted)
            {
                _server.Stop();
                _server.Dispose();
            }
        }
    }
}