﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MockServer.Providers.WireMockProxy
{
    public interface IWireMockClient
    {
        HttpResponseMessage GetResponse(HttpRequestMessage requestMessage);
    }
}