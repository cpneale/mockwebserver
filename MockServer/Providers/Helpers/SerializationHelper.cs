﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MockServer.Providers.Helpers
{
    public class SerializationHelper
    {
        private static string SerialiseObject<TRequest>(TRequest xmlObject)
        {
            var serializer = new XmlSerializer(typeof(TRequest));

            using (var stringWriter = new StringWriterWithEncoding(Encoding.UTF8))
            {
                using (var xmlTextWriter = new XmlTextWriter(stringWriter))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;

                    serializer.Serialize(xmlTextWriter, xmlObject);

                    return stringWriter.ToString();
                }
            }
        }
        /// <summary>
        /// This allows the text encoding to be specified for a stringwriter. Necessary because without the
        /// encoding defaults to UTF-16, the QCL API only handles UTF-8.
        /// </summary>
        private sealed class StringWriterWithEncoding : StringWriter
        {
            public override Encoding Encoding { get; }

            public StringWriterWithEncoding(Encoding encoding)
            {
                Encoding = encoding;
            }
        }
    }
}