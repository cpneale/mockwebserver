﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace MockServer.Providers.Helpers
{
    public class HttpRequestHelper
    {
        public static HttpRequestMessage CloneHttpRequestMessage(HttpRequestMessage req, Uri replacementUri)
        {
            req.Content.LoadIntoBufferAsync().Wait();

            var clone = new HttpRequestMessage(req.Method, replacementUri);

            SetContent(req, clone);

            clone.Version = req.Version;
            clone.Headers.Host = replacementUri.Authority;

            foreach (KeyValuePair<string, object> prop in req.Properties)
                clone.Properties.Add(prop);

            foreach (KeyValuePair<string, IEnumerable<string>> header in req.Headers)
                clone.Headers.TryAddWithoutValidation(header.Key, header.Value);

            return clone;
        }
        private static void SetContent(HttpRequestMessage incomingRequest, HttpRequestMessage clonedRequest)
        {
            var verb = incomingRequest.Method;
            if (verb == HttpMethod.Post || verb == HttpMethod.Put || verb == HttpMethod.Delete)
            {
                var ms = new MemoryStream();
                if (incomingRequest.Content != null)
                {
                    incomingRequest.Content.CopyToAsync(ms).Wait();
                    ms.Position = 0;
                    clonedRequest.Content = new StreamContent(ms);

                    if (incomingRequest.Content.Headers != null)
                    {
                        foreach (var h in incomingRequest.Content.Headers)
                        {
                            if (h.Key != "Content-Length")
                                clonedRequest.Content.Headers.Add(h.Key, h.Value);
                        }
                    }
                }
            }
        }
    }
}