﻿using System;

namespace MockServer.Providers.Exceptions
{
    public class WireMockMappingException : Exception
    {
        public WireMockMappingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
