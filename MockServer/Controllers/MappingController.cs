﻿using System;
using System.Net.Http;
using System.Web.Http;
using MockServer.Services;

namespace MockServer.Controllers
{
    public class MappingController : ApiController
    {
        private IHttpResponseService _httpResponseService;

        public MappingController(IHttpResponseService httpResponseService)
        {
            _httpResponseService = httpResponseService;
        }

        [HttpGet, Route("{*url}")]
        public HttpResponseMessage GetResponseToGet()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpPost, Route("{*url}")]
        public HttpResponseMessage GetResponseToPost()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpPut, Route("{*url}")]
        public HttpResponseMessage GetResponseToPut()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpDelete, Route("{*url}")]
        public HttpResponseMessage GetResponseToDelet()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpPatch, Route("{*url}")]
        public HttpResponseMessage GetResponseToPatch()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpHead, Route("{*url}")]
        public HttpResponseMessage GetResponseToHead()
        {
            return _httpResponseService.GetResponse(base.Request);
        }

        [HttpOptions, Route("{*url}")]
        public HttpResponseMessage GetResponseToOptions()
        {
            return _httpResponseService.GetResponse(base.Request);
        }


        ////Qcl Api
        //[HttpPost, Route("device/registeredDevices/retrieve")]
        //public HttpResponseMessage RetrieveRegisteredDevices()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("device/retrieve")]
        //public HttpResponseMessage RetrieveDevice()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("device/passthroughs/update")]
        //public HttpResponseMessage UpdateDevicePassthrough()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("hub/retrieve")]
        //public HttpResponseMessage RetrieveHub()
        //{
        //    return GetResponse();
        //}

        //[HttpPut, Route("hub/retrieve")]
        //public HttpResponseMessage AddOrUpdateHub()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("hub/timezone/update")]
        //public HttpResponseMessage UpdateHubTimeZone()
        //{
        //    return GetResponse();
        //}


        //[HttpPut, Route("hub/activation")]
        //public HttpResponseMessage AddOrUpdateHubActivation()
        //{
        //    return GetResponse();
        //}


        ////admin mappings
        //[HttpPost, Route("__admin/mappings")]
        //public HttpResponseMessage AddAdminMapping()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("__admin/mappings/reset")]
        //public HttpResponseMessage DeleteAllAdminMappings()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("__admin/mappings/save")]
        //public HttpResponseMessage SaveAllAdminMappings()
        //{
        //    return GetResponse();
        //}

        //[HttpDelete, Route("__admin/mappings/{mappingGuid}")]
        //public HttpResponseMessage DeleteAdminMapping(Guid mappingGuid)
        //{
        //    return GetResponse();
        //}

        //[HttpPut, Route("__admin/mappings/{mappingGuid}")]
        //public HttpResponseMessage UpdateAdminMapping(Guid mappingGuid)
        //{
        //    return GetResponse();
        //}

        //[HttpGet, Route("__admin/mappings")]
        //public HttpResponseMessage GetAdminMappings()
        //{
        //    return GetResponse();
        //}

        //[HttpGet, Route("__admin/mappings/{mappingGuid}")]
        //public HttpResponseMessage GetAdminMapping(Guid mappingGuid)
        //{
        //    return GetResponse();
        //}


        ////admin settings
        //[HttpGet, Route("__admin/settings")]
        //public HttpResponseMessage GetAdminSettings()
        //{
        //    return GetResponse();
        //}

        //[HttpPost, Route("__admin/settings")]
        //public HttpResponseMessage AddAdminSetting()
        //{
        //    return GetResponse();
        //}


        ////admin requests
        //[HttpGet, Route("__admin/requests")]
        //public HttpResponseMessage GetAdminRequests()
        //{
        //    return GetResponse();
        //}

    }
}
