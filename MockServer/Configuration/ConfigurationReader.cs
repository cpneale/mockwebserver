﻿using System.Configuration;

namespace MockServer.Configuration
{
    public class ConfigurationReader : IConfigurationReader
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}