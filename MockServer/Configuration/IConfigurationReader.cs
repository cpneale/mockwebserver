﻿namespace MockServer.Configuration
{
    public interface IConfigurationReader
    {
         string Get(string key);
    }
}