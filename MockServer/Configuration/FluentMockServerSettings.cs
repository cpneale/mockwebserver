﻿using System;
using WireMock.Handlers;
using WireMock.Logging;
using WireMock.Settings;

namespace MockServer.Configuration
{
    public class FluentMockServerSettings : IFluentMockServerSettings
    {
        public FluentMockServerSettings(IConfigurationReader configReader)
        {
            if(configReader == null)
                throw new ArgumentNullException(nameof(configReader));

            _configurationReader = configReader;
        }

        public int? Port
        {
            get
            {
                int rslt;
                int.TryParse(_configurationReader.Get("WireMockPort"), out rslt);
                return rslt;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string[] Urls
        {
            get
            {
              return  _configurationReader.Get("WireMockUrl").Split();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int StartTimeout
        {
            get
            {
                int rslt;
                int.TryParse(_configurationReader.Get("WireMockStartTimeout"), out rslt);
                return rslt;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool? StartAdminInterface
        {
            get
            {
                bool rslt;
                bool.TryParse(_configurationReader.Get("WireMockStartAdminInterface"), out rslt);
                return rslt;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool? ReadStaticMappings
        {
            get
            {
                bool rslt;
                bool.TryParse(_configurationReader.Get("WireMockReadStaticMappings"), out rslt);
                return rslt;
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public bool? AllowPartialMapping
        {
            get
            {
                bool rslt;
                bool.TryParse(_configurationReader.Get("WireMockAllowPartialMapping"), out rslt);
                return rslt;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool? UseSSL { get; set; }

        public bool? WatchStaticMappings { get; set; }
        public IProxyAndRecordSettings ProxyAndRecordSettings { get; set; }
        public string AdminUsername { get; set; }
        public string AdminPassword { get; set; }
        public int? RequestLogExpirationDuration { get; set; }
        public int? MaxRequestLogCount { get; set; }
        public Action<object> PreWireMockMiddlewareInit { get; set; }
        public Action<object> PostWireMockMiddlewareInit { get; set; }
        public IWireMockLogger Logger { get; set; }
        public IFileSystemHandler FileSystemHandler { get; set; }

        private readonly IConfigurationReader _configurationReader;
    }   
}