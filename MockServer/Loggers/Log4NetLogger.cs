﻿using System;
using WireMock.Admin.Requests;
using WireMock.Logging;
using System.Diagnostics;
using log4net;

namespace MockServer.Loggers
{
    public class Log4NetLogger : IWireMockLogger
    {
        private ILog _logger;

        public Log4NetLogger(ILog log4NetLog)
        {
            _logger = log4NetLog;
        }
        public void Debug(string formatString, params object[] args)
        {
            _logger.Debug(string.Format(formatString, args));
        }

        public void Info(string formatString, params object[] args)
        {
            _logger.Info(string.Format(formatString, args));
        }

        public void Warn(string formatString, params object[] args)
        {
            _logger.Warn(string.Format(formatString, args));
        }

        public void Error(string formatString, params object[] args)
        {
            _logger.Error(string.Format(formatString, args));
        }

        public void DebugRequestResponse(LogEntryModel logEntryModel, bool isAdminRequest)
        {
            _logger.Info($"Request Received:" +
                         $"{Environment.NewLine}" +
                         $"{logEntryModel.Request}" +
                         $"{Environment.NewLine}" +
                         $"{logEntryModel.Response}" +
                         $"{logEntryModel.RequestMatchResult}");
        }
    }
}