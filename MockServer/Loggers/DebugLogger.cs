﻿using WireMock.Admin.Requests;
using WireMock.Logging;
using System.Diagnostics;

namespace MockServer.Loggers
{
    public class DebugLogger : IWireMockLogger
    {
        public void Debug(string formatString, params object[] args)
        {
            Debugger.Log(1, "Debug", formatString);
        }

        public void Info(string formatString, params object[] args)
        {
            Debugger.Log(2, "Info", formatString);
        }

        public void Warn(string formatString, params object[] args)
        {
            Debugger.Log(3, "Warn", formatString);
        }

        public void Error(string formatString, params object[] args)
        {
            Debugger.Log(4, "Error", formatString);
        }

        public void DebugRequestResponse(LogEntryModel logEntryModel, bool isAdminRequest)
        {
            Debugger.Log(5, "DebugRequestResponse", $"{logEntryModel.ToString()}.  IsAdmin={isAdminRequest}");
        }
    }
}