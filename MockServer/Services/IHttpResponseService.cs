﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MockServer.Services
{
    public interface IHttpResponseService
    {
        HttpResponseMessage GetResponse(HttpRequestMessage httpRequestMessage);
    }
}