﻿using System.Net.Http;
using System.Threading.Tasks;

using MockServer.Providers.WireMockProxy;

namespace MockServer.Services
{
    public class HttpResponseService : IHttpResponseService
    {
        private readonly IWireMockClient _wireMockClient;

        public HttpResponseService(IWireMockClient wireMockClient)
        {
            _wireMockClient = wireMockClient;
        }

        public HttpResponseMessage GetResponse(HttpRequestMessage httpRequestMessage)
        {
            return _wireMockClient.GetResponse(httpRequestMessage);
        }
    }
}
